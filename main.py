from grammar import Grammar
from parse_table import ParseTable
from parsing import Parser

if __name__ == '__main__':
    # s = Scanner('tokens.txt')
    # for q in s.scan(open('test.c').read()):
    #     print(q)
    #

    g = Grammar()
    g.read('grammar.txt')
    # g.log_productions()
    # g.convert_to_jflap('grammar_out.jff')

    pt = ParseTable('parse_table.csv', g)
    p = Parser(pt, g)
    p.parse('test.c')
