from scanner import Scanner
from semantics import SemanticAnalyzer


class Parser:
    def __init__(self, parse_table, grammar):
        self.parse_table = parse_table
        self.grammar = grammar

        self.semantics = SemanticAnalyzer()

        self.stack = [0]

    def parse(self, file_path):
        s = Scanner('tokens.txt')
        tokens = s.scan(open(file_path).read())

        done = False

        lookahead = next(tokens)

        while not done:
            action = self.parse_table[self.stack[-1], lookahead['name']]

            if action[0] == 's':
                new_state = int(action[1:])
                self.stack.append(lookahead)
                self.stack.append(new_state)
                # print('Shift {}({}) {}'.format(lookahead['name'], lookahead['lexeme'], new_state))
                lookahead = next(tokens)
            elif action[0] == 'r':
                production_no = int(action[1:])
                production = self.grammar.get_production(production_no)
                lhs = production.left[1]
                rhs = production.right[1]
                # print('Reduce {} -> {}'.format(lhs, rhs))

                stack_tokens = []

                for token in reversed(rhs):
                    if token == 'ε':
                        continue
                    self.stack.pop()
                    stack_token = self.stack.pop()
                    stack_tokens.append(stack_token)
                    if stack_token['name'] != token:
                        print('NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')
                        print(stack_token['name'], token)
                        exit(1)

                stack_tokens.reverse()

                new_token = {
                    'name': lhs,
                    'children': stack_tokens,
                }

                new_state = self.parse_table[self.stack[-1], lhs]
                # print('\tPrev state: {}'.format(self.stack[-1]))
                self.stack.append(new_token)
                self.stack.append(new_state)

                # print('\tGo to state {}'.format(new_state))
            elif action == 'acc':
                # Analyze the parse tree
                done = True
                self.semantics.analyze(self.stack[1])
