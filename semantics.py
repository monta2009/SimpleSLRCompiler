from collections import defaultdict
from contextlib import contextmanager, suppress

from utils import log_purple


class Symbol:
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

        # if self.symbol_type.startswith('var'):
        #     log_blue('{} {} {} {} {}'.format(self.name, self.scope, self.symbol_type, self.var_type, self.size))

    def __repr__(self):
        res = '{} {} {}'.format(self.scope, self.symbol_type, self.name)

        if self.symbol_type == 'var':
            res += ' {} {}'.format(self.var_type, self.size)
        elif self.symbol_type == 'func':
            res += ' {} ({})'.format(self.ret_type, ', '.join(self.params))

        return res


class Instruction:
    def __init__(self, op, *addresses):
        self.op = op
        self.addresses = addresses

        # log_green(self)

    def __repr__(self):
        return '({})'.format(', '.join([self.op, *(str(s) for s in self.addresses)]))


class MemoryManager:
    def __init__(self):
        self.base_address = 1000

    def new_var(self, size=1):
        ret = self.base_address
        self.base_address += 4 * size
        return ret


class SemanticAnalyzer:
    @contextmanager
    def new_scope(self):
        self._enter_scope()
        yield
        self._exit_scope()

    def __init__(self):
        self.symbol_table = defaultdict(list)
        self.current_scope = 0

        self.code = []
        self.semantic_stack = []

        self.mem_mgr = MemoryManager()
        self.current_func = None

    def analyze(self, parse_tree):
        self._analyze(parse_tree)

        f = open('output.txt', 'w')
        for i, c in enumerate(self.code):
            f.write('{}\t{}\n'.format(i, c))

    def _analyze(self, tree):
        name = tree['name']
        # print('{} in scope {}'.format(name, self.current_scope))

        if name == 'program':
            with self.new_scope():
                self.code.append(None)  # jump to main()
                for node in tree['children']:
                    self._analyze(node)

                main_func = self._look_up('main')

                if main_func.symbol_type != 'func':
                    raise Exception('Symbol main defined, but not callable!')
                    # TODO

                self.code[0] = Instruction('JP', main_func.mem_loc)
        elif name == 'var-declaration':
            self._parse_var_decl(tree)
        elif name == 'fun-declaration':
            self._parse_func(tree)
        else:
            if 'children' in tree:
                for node in tree['children']:
                    self._analyze(node)

    def _parse_var_decl(self, tree):
        children = tree['children']
        var_type = children[0]['children'][0]['name']
        name = children[1]['lexeme']
        size = 0
        if children[2]['name'] == 'LBRACE':
            # is array
            size = int(children[3]['lexeme'])
        mem_loc = self.mem_mgr.new_var(size if size else 1)
        self._declare_id(name, 'var', var_type=var_type, size=size, mem_loc=mem_loc)

    def _parse_func(self, tree):
        children = tree['children']
        ret_type = children[0]['children'][0]['name']
        name = children[1]['lexeme']

        func_symbol = self._declare_id(name, 'func', ret_type=ret_type, params=[])

        self.current_func = name

        with self.new_scope():
            params = self._parse_params(children[3])
            func_symbol.mem_loc = len(self.code)
            func_symbol.return_pointer = self.mem_mgr.new_var()
            func_symbol.return_value = self.mem_mgr.new_var()
            self._parse_cmpnd_stmt(children[5], False)
            if self.current_func != 'main':
                self.code.append(Instruction('JP', '@{}'.format(func_symbol.return_pointer)))

        self.current_func = None

        func_symbol.params = params

    def _parse_cmpnd_stmt(self, tree, create_scope=True):
        ctx_mgr = self.new_scope() if create_scope else suppress()
        with ctx_mgr:
            children = tree['children']

            local_decl = children[1]
            self._parse_local_decl(local_decl)

            stmt_list = children[2]
            self._parse_stmt_list(stmt_list)

    def _parse_stmt_list(self, tree):
        done = False
        to_parse = []
        while not done:
            if 'children' not in tree or len(tree['children']) != 2:
                done = True
            else:
                to_parse.append(tree['children'][1])
                tree = tree['children'][0]
        to_parse.reverse()
        for stmt in to_parse:
            self._parse_stmt(stmt)

    def _parse_stmt(self, tree):
        stmt_parsers = {
            'expression-stmt': self._parse_exp_stmt,
            'compound-stmt': self._parse_cmpnd_stmt,
            'selection-stmt': self._parse_sel_stmt,
            'iteration-stmt': self._parse_iter_stmt,
            'return-stmt': self._parse_ret_stmt,
        }

        real_stmt = tree['children'][0]
        stmt_parsers[real_stmt['name']](real_stmt)

    def _parse_exp_stmt(self, tree):
        if len(tree['children']) == 1:
            # single semicolon
            pass
        else:
            self._parse_exp(tree['children'][0], False)

    def _parse_exp(self, tree, needs_result):
        # print('\t#PARSE EXP')
        if tree['children'][0]['name'] == 'var':
            if needs_result:
                raise Exception('Assign expression has no value!')
                # TODO: what to do?
            self._parse_var(tree['children'][0])
            self._parse_exp(tree['children'][2], True)
            self.code.append(Instruction('ASSIGN', self.semantic_stack.pop(), self.semantic_stack.pop()))
        else:
            self._parse_simple_exp(tree['children'][0], needs_result)

    def _parse_var(self, tree):
        children = tree['children']
        var = self._look_up(children[0]['lexeme'])
        if not var.symbol_type.startswith('var'):
            raise Exception('{} used as variable: {}!'.format(var.symbol_type, children[0]['lexeme']))
            # TODO: what to do?
        if len(children) == 1:
            self.semantic_stack.append(var.mem_loc)
        else:
            if var.size == 0:
                raise Exception('not an array: {}!'.format(children[0]['lexeme']))
                # TODO: what to do?
            self._parse_exp(children[2], True)
            tmp = self.mem_mgr.new_var()
            array_start = '{op}{mem_loc}'.format(op='' if var.symbol_type == '' else '#', mem_loc=var.mem_loc)
            self.code.append(Instruction('ADD',
                                         array_start,  # Address of array
                                         self.semantic_stack.pop(),  #
                                         tmp)  # Address of array element
                             )
            self.semantic_stack.append('@{}'.format(tmp))

    def _parse_simple_exp(self, tree, needs_result):
        children = tree['children']
        if len(children) == 1:
            self._parse_add_exp(children[0], needs_result)
        else:
            self._parse_add_exp(children[0], needs_result)
            self._parse_add_exp(children[2], needs_result)

            operator = children[1]['children'][0]['name']  # LT|EQ
            tmp = self.mem_mgr.new_var()

            self.code.append(Instruction(operator, self.semantic_stack.pop(-2), self.semantic_stack.pop(),
                                         tmp))
            self.semantic_stack.append(tmp)

    def _parse_add_exp(self, tree, needs_result):
        children = tree['children']
        if len(children) == 1:
            self._parse_term(children[0], needs_result)
        else:
            self._parse_add_exp(children[0], needs_result)
            self._parse_term(children[2], needs_result)
            operator = 'ADD' if children[1]['children'][0]['name'] == 'PLUS' else 'SUB'
            tmp = self.mem_mgr.new_var()

            self.code.append(Instruction(operator, self.semantic_stack.pop(-2), self.semantic_stack.pop(),
                                         tmp))
            self.semantic_stack.append(tmp)

    def _parse_term(self, tree, needs_result):
        children = tree['children']
        if len(children) == 1:
            self._parse_factor(children[0], needs_result)
        else:
            self._parse_term(children[0], needs_result)
            self._parse_factor(children[2], needs_result)

            tmp = self.mem_mgr.new_var()

            self.code.append(Instruction('MULT', self.semantic_stack.pop(), self.semantic_stack.pop(),
                                         tmp))
            self.semantic_stack.append(tmp)

    def _parse_factor(self, tree, needs_result):
        children = tree['children']
        if len(children) == 3:  # ( expression )
            self._parse_exp(children[1], True)
        else:
            if children[0]['name'] == 'var':
                self._parse_var(children[0])
            elif children[0]['name'] == 'call':
                self._parse_call(children[0], needs_result)
            elif children[0]['name'] == 'NUM':
                self.semantic_stack.append('#{}'.format(children[0]['lexeme']))

    def _parse_call(self, tree, needs_result):
        children = tree['children']
        func_name = children[0]['lexeme']
        args_count = self._parse_args(children[2])
        if func_name == 'output':
            if args_count != 1:
                raise Exception('Function "output" needs 1 arg(s) called with {}!'.format(args_count))
                # TODO: what to do?
            self.code.append(Instruction('PRINT', self.semantic_stack.pop()))
        else:
            func = self._look_up(func_name)
            if func.symbol_type != 'func':
                raise Exception('Symbol is not callable: {}!'.format(func.name))
                # TODO: what to do?
            if args_count != len(func.params):
                raise Exception(
                    'Function "{}" needs {} arg(s) called with {}!'.format(func.name, len(func.params), args_count))
                # TODO: what to do?

            if func.ret_type == 'VOID':
                if needs_result:
                    raise Exception(
                        'Function {} with void return type called expecting return value!'.format(func.name))
                    # TODO: what to do?

            for p in reversed(func.params):
                if p.symbol_type == 'VAR':
                    self.code.append(Instruction('ASSIGN', self.semantic_stack.pop(), p.mem_loc))
                else:  # VAR[]
                    self.code.append(Instruction('ASSIGN', self.semantic_stack.pop(), '#{}'.format(p.mem_loc)))

            self.code.append(Instruction('ASSIGN', '#{}'.format(len(self.code) + 2), func.return_pointer))
            self.code.append(Instruction('JP', func.mem_loc))
            self.semantic_stack.append(func.return_value)

    def _parse_args(self, tree):
        # print('\t#PARSE ARGS')
        arg_count = 0
        children = tree['children']
        for ch in children:
            if ch['name'] == 'expression':
                self._parse_exp(ch, True)
                arg_count += 1
            elif ch['name'] == 'arg-list':
                arg_count += self._parse_args(ch)
        return arg_count

    def _parse_sel_stmt(self, tree):
        self._parse_exp(tree['children'][2], True)
        self.code.append(None)  # placeholder
        placeholder = len(self.code) - 1
        condition = self.semantic_stack.pop()
        self._parse_stmt(tree['children'][4])
        self.code.append(None)  # placeholder2
        placeholder2 = len(self.code) - 1
        self.code[placeholder] = Instruction('JPF', condition, len(self.code))
        self._parse_stmt(tree['children'][6])
        self.code[placeholder2] = Instruction('JP', len(self.code))

    def _parse_iter_stmt(self, tree):
        before_while = len(self.code)
        self._parse_exp(tree['children'][2], True)
        condition = self.semantic_stack.pop()
        self.code.append(None)  # placeholder
        placeholder = len(self.code) - 1
        self._parse_stmt(tree['children'][4])
        self.code.append(Instruction('JP', before_while))
        self.code[placeholder] = Instruction('JPF', condition, len(self.code))

    def _parse_ret_stmt(self, tree):
        if len(tree['children']) == 2:
            # empty return
            pass
        else:
            self._parse_exp(tree['children'][1], True)
            func = self._look_up(self.current_func)
            self.code.append(Instruction('ASSIGN', self.semantic_stack.pop(), func.return_value))

    def _parse_local_decl(self, tree):
        done = False
        to_parse = []
        while not done:
            if 'children' not in tree or len(tree['children']) != 2:
                done = True
            else:
                to_parse.append(tree['children'][1])
                tree = tree['children'][0]
        to_parse.reverse()
        for p in to_parse:
            self._parse_var_decl(p)

    def _parse_params(self, tree):
        children = tree['children']
        func_params = []
        for ch in children:
            if ch['name'] == 'param':
                param = ch['children']
                var_type = param[0]['children'][0]['name']
                name = param[1]['lexeme']
                size = 0
                mem_loc = self.mem_mgr.new_var()
                if len(param) > 2:
                    # is array with unknown size
                    size = -1
                    func_params.append(
                        self._declare_id(name, 'var[]', var_type=var_type, size=size, mem_loc=mem_loc)
                    )
                else:
                    func_params.append(
                        self._declare_id(name, 'var', var_type=var_type, size=size, mem_loc=mem_loc)
                    )

            elif ch['name'] == 'param-list':
                func_params.extend(self._parse_params(ch))

        return func_params

    def _enter_scope(self):
        self.current_scope += 1

    def _declare_id(self, name, symbol_type, **kwargs):
        symbol = self._look_up(name, error=False)

        if symbol is not None and symbol.scope == self.current_scope:
            raise Exception('redefinition of {}!'.format(name))
            # TODO: what to do?

        new_symbol = None

        if symbol_type == 'var':
            new_symbol = Symbol(name=name, scope=self.current_scope, symbol_type=symbol_type,
                                var_type=kwargs.get('var_type'),
                                size=kwargs.get('size'),
                                mem_loc=kwargs.get('mem_loc'))
        elif symbol_type == 'func':
            new_symbol = Symbol(name=name, scope=self.current_scope, symbol_type=symbol_type,
                                ret_type=kwargs.get('ret_type'),
                                params=kwargs.get('params'))

        if new_symbol is not None:
            self.symbol_table[name].append(new_symbol)

        # self.log_symbols()

        return new_symbol

    def _look_up(self, name, error=True):
        symbols = self.symbol_table[name]
        if len(symbols):
            return symbols[-1]
        if error:
            raise Exception('Undefined symbol {}!'.format(name))
            # TODO: what to do?
        return None

    def _exit_scope(self):
        for name in list(self.symbol_table.keys()):
            id_row = self.symbol_table[name]
            if len(id_row) and id_row[-1].scope == self.current_scope:
                id_row.pop()
            if len(id_row) == 0:
                del self.symbol_table[name]
        self.current_scope -= 1

    def log_symbols(self):
        log_purple('-' * 25)
        for name in self.symbol_table:
            for sym in self.symbol_table[name]:
                log_purple('{}{}'.format(' ' * (sym.scope - 1), sym))
        log_purple('-' * 25)
