class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def log_blue(*args):
    print(bcolors.OKBLUE, *args, bcolors.ENDC)


def log_green(*args):
    print(bcolors.OKGREEN, *args, bcolors.ENDC)


def log_purple(*args):
    print(bcolors.HEADER, *args, bcolors.ENDC)


def log_error(*args):
    print(bcolors.FAIL, *args, bcolors.ENDC)
