import csv

from collections import defaultdict


class ParseTable():
    def __init__(self, file_path, grammer):
        self._table = defaultdict(dict)

        f = open(file_path)
        reader = csv.reader(f)

        for row in reader:
            state = int(row[0])
            # print(row)

            i = 1
            for term in grammer.terms:
                # print(term, row[i])
                self._table[state][term] = row[i]
                i += 1
            for non_term in grammer.non_terms:
                # print(non_term, row[i])
                self._table[state][non_term] = row[i]
                i += 1

            # print(state, self._table[state])

    def __getitem__(self, item):
        state = item[0]
        symbol = item[1]

        res = self._table[state][symbol]

        return int(res) if res.isdigit() else res
