NON_TERM_CHARS = sorted('ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂǍĂÃẢȦẠÄÅḀĀĄȀẤẦẪẨẬẮẰẴ')
TERM_CHARS = sorted('abcdefghijklmnopqrstuvwxyzàáâǎăãảȧạäåḁāą')


class Production:
    def __init__(self, left, right, no):
        self.left = left
        self.right = right
        self.no = no

    def __repr__(self):
        lhs = '<left>{left}</left>'.format(left=self.left[0])
        rhs = '<right>{right}</right>'.format(right=self.right[0]) if self.right[0] != 'ε' else '<right/>'
        return '''
    <production>
        {lhs}
        {rhs}
    </production>'''.format(lhs=lhs, rhs=rhs)


class Grammar:
    def __init__(self):
        self.non_terms = None
        self.terms = None
        self.non_terms_mapping = None
        self.terms_mapping = None
        self.productions = None

    def read(self, file_path):
        f = open(file_path, encoding='utf8')

        self.non_terms_mapping = {}
        self.terms_mapping = {}
        self.productions = []
        self.non_terms = []
        self.terms = []

        for l in f:
            left, right = (s.strip() for s in l.split('→'))

            if left not in self.non_terms_mapping:
                self.non_terms.append(left)
                self.non_terms_mapping[left] = NON_TERM_CHARS[len(self.non_terms_mapping)]

            right_parts = (s.strip() for s in right.split('|'))
            for part in right_parts:
                rhs = ''
                for word in part.split():
                    if word == 'ε':
                        rhs = 'ε'
                        continue

                    if not word[0].isalpha() or word.isupper():
                        # terminal
                        if word not in self.terms_mapping:
                            self.terms_mapping[word] = TERM_CHARS[len(self.terms_mapping)]
                            self.terms.append(word)
                        rhs += self.terms_mapping[word]
                    else:
                        # non-terminal
                        if word not in self.non_terms_mapping:
                            self.non_terms_mapping[word] = NON_TERM_CHARS[len(self.non_terms_mapping)]
                            self.non_terms.append(word)
                        rhs += self.non_terms_mapping[word]

                self.productions.append(
                    Production((self.non_terms_mapping[left], left), (rhs, part.split()), len(self.productions) + 1))

        self.terms.append('$')

    def get_production(self, prod_no):
        return self.productions[prod_no - 1]

    def log_productions(self):
        for p in self.productions:
            print('{}. {} -> {}'.format(p.no, p.left[1], p.right[1]))

    def convert_to_jflap(self, output_path):
        f = open(output_path, 'w')
        prod_list = ''.join(repr(s) for s in self.productions)
        f.write("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<structure>
    <type>grammar</type>
    <!--The list of productions.-->
    {prod_list}
</structure>""".format(prod_list=prod_list))
