import re


class Scanner:
    def __init__(self, token_conf_path=None, panic_mode=False):
        self._regex_cache = None
        self.token_conf_path = token_conf_path
        self.panic_mode = panic_mode

    @property
    def regex(self):
        if self._regex_cache is None:
            if self.token_conf_path is None:
                raise ValueError('Token config file path is not set!')

            regexes = []
            token_conf = open(self.token_conf_path)
            for l in token_conf:
                if l.startswith('#'):
                    # Comment! Skip line.
                    continue
                name, pattern = (s.strip() for s in l.split(maxsplit=1))
                regexes.append('(?P<{name}>{pattern})'.format(name=name, pattern=pattern))

            self._regex_cache = re.compile('|'.join(regexes))
        return self._regex_cache

    def set_tokens(self, token_conf_path):
        self.token_conf_path = token_conf_path
        self._regex_cache = None

    def scan(self, text):
        tokens = self.regex.finditer(text)
        line_number = 1
        line_start = 0
        for token in tokens:
            name = token.lastgroup
            lexeme = token.group(name)
            if name == 'NEWLINE':
                line_number += 1
                line_start = token.end()
            elif name == 'ERROR':
                print('Input Error! Unscannable Token "{}" at {}:{}.'.format(lexeme, line_number,
                                                                             token.start() - line_start + 1))
                if not self.panic_mode:
                    raise ValueError('Unscannable Token!')

            elif token.lastgroup not in ['IGNORE', 'COMMENT']:
                yield ({
                    'name': name,
                    'lexeme': lexeme,
                    'line_no': line_number,
                    'pos': token.start() - line_start + 1,
                })
        while True:
            yield ({
                'name': '$',
                'lexeme': '',
                'line_no': line_number,
                'pos': -1,
            })
